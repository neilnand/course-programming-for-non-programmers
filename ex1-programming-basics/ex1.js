
// // BLOCK 1
// // assignment

// var myVar = "Hello";
// console.log(myVar);

// // END BLOCK 1





// // BLOCK 2
// // IF statement

// var time = 1;

// if (time < 12) {

// 	console.log("Morning");

// } else if (time == 12) {

// 	console.log("Noon");

// } else if (time > 12) {

// 	console.log("Afternoon");

// }

// // END BLOCK 2





// // BLOCK 3
// // LOOP statement

// for (var i = 0; i < 10; i++) {

// 	console.log(i);

// }

// // END BLOCK 3





// // BLOCK 4
// // Functions

// function makeFruit() {

// 	console.log( "I'm a fruit" );

// }

// makeFruit();

// // END BLOCK 4





// // BLOCK 5
// // Objects

// function Fruit(type) {

// 	// Properties
// 	this.type = type;
// 	this.quantity = 0;

// 	// Methods
// 	this.add = function () {
// 		this.quantity++;
// 	}
// 	this.remove = function () {
// 		this.quantity--;
// 	}

// 	console.log(this);

// }

// var a = new Fruit("apple");


// // END BLOCK 5





// // BLOCK 6
// // Advanced Assignment

// var foo = 1;
// var bar = foo;

// console.log(bar);

// foo = 0;

// console.log(bar);

// console.log(foo == bar);

// // --

// // function Couch() {
// // 	this.colour = "red";
// // }

// // var couch1 = new Couch();
// // var couch2 = couch1

// // console.log(couch2);

// // couch1.colour = "blue";

// // console.log(couch2);

// // console.log(couch1 == couch2);



// // END BLOCK 6